#include <iostream>
#include <string>
#include <functional>

int hashingFunction(std::string key){
	std::hash<std::string> hashFunc;
	int hash = hashFunc(key);
	return hash;
}

int main(int argc, char* argv[]){
	int hash = hashingFunction("Hash Key");
	std::cout << hash << std::endl;
}