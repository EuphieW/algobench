#!/bin/bash
algo_path="$(dirname $0)"

algo_save_dir=$algo_path/saved/
algo_output_dir=$algo_path/output/
algo_input_dir=$algo_path/input/

if [ ! -d "$algo_input_dir" ]; then
        mkdir "$algo_input_dir"
	echo "Created \"input\" dir"
fi
if [ ! -d "$algo_output_dir" ]; then
        mkdir "$algo_output_dir"
        echo "Created \"output\" dir"
fi
if [ ! -d "$algo_save_dir" ]; then
        mkdir "$algo_save_dir"
        echo "Created \"save\" dir"
fi

echo Launching algobench
# make it executable
chmod a+x $algo_path/algobench_b
# go
java -jar $algo_path/algobench.jar $algo_path $algo_path/algobench_b
echo Exited
